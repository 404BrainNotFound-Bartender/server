from django.core.validators import validate_comma_separated_integer_list
from django.db import models
import uuid
import json

# Create your models here.


class Pump(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    pin = models.IntegerField()
    objects = models.manager

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "pin": self.pin
        }


class Ingredient(models.Model):
    # System information
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pid_p = models.FloatField(default=1.0)
    pid_i = models.FloatField(default=1.0)
    pid_d = models.FloatField(default=1.0)
    assigned_pump = models.ForeignKey(Pump, on_delete=models.CASCADE, blank=True, null=True)
    attached_to_system = models.BooleanField()
    est_remaining = models.FloatField()

    # Ingredient Information
    name = models.CharField(max_length=200)
    maker = models.CharField(max_length=200, default="")
    density = models.FloatField()
    proof = models.FloatField()
    alternatives = models.ManyToManyField("self")
    image = models.CharField(validators=[validate_comma_separated_integer_list], max_length=250000, default="")

    def to_json(self):
        return json.dumps(self.to_dic())

    def to_dic(self):
        return {
            "id": self.id,
            "name": self.name,
            "maker": self.maker,
            "density": self.density,
            "attached": self.attached_to_system,
            "remaining": self.est_remaining,
            "proof": self.proof,
            "alternatives": [ingre.id for ingre in self.alternatives.all()],
        }

    def __str__(self):
        return self.name

    objects = models.Manager


class Component(models.Model):
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    amount = models.FloatField()


class RecipeRating(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rating = models.FloatField(default=0)
    objects = models.Manager


class Recipe(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    ingredients = models.ManyToManyField(Component)
    severing_size = models.FloatField(default=12)
    density = models.FloatField(default=1)
    image = models.CharField(validators=[validate_comma_separated_integer_list], max_length=250000, default="")
    ratings = models.ManyToManyField(RecipeRating)

    objects = models.Manager

    def __str__(self):
        return self.name


class MakeRecipeRequest(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.FloatField()
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    made = models.BooleanField(default=False)
    making = models.BooleanField(default=False)
    positions_in_queue = models.IntegerField(default=0)

    objects = models.Manager

    def __str__(self):
        return id

