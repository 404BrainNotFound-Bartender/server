from django.urls import path
from . import views

urlpatterns = [
    path('addIngredient/', views.add_ingredient, name="addIngredient"),
    path('getIngredients/', views.get_ingredients, name="getIngredients"),
    path("addRecipe/", views.add_recipe, name="addRecipe"),
    path("getRecipes/", views.get_all_recipes, name="getRecipes"),
    path("makeRecipe/<uuid:recipe_id>/<int:amount>/",
         views.make_recipe, name="makeRecipe"),
    path("saveRecipe/", views.update_recipe, name="saveRecipe"),
    path("getImage/<int:type>/<uuid:id>/", views.get_image, name="getImage"),
    path("saveIngredient/<uuid:id>/", views.save_ingredient, name="saveIngredient"),
    path("getPumps/", views.get_pumps, name="getPumps"),
    path("drinkStatus/<uuid:id>/", views.get_drink_status, name="drinkStatus"),
    path("dispenseIngredient/<uuid:manual_code>/<uuid:ingredient>/<int:amount>/",
         views.dispense_ingredient, name="dispenseIngredient"),
    path("enterManualMode/", views.enter_manual_mode, name="enterManualMode"),
    path("leaveManualMode/<uuid:manual_code>/", views.leave_manual_mode, name="leaveManualMode"),
    path("trainIngredientPID/<uuid:ingredient_id>/<int:number_of_cycle>/<int:size_of_glass>/",
         views.train_ingredient_pid, name="trainIngredientPID"),
    path("deleteRecipe/<uuid:recipe_id>/", views.delete_recipe, name="deleteRecipe"),
    path("rateRecipe/<uuid:recipe_id>/<int:rating>/", views.rate_recipe, name="rateRecipe"),
    path("testConnection/", views.connection_test, name="connectTest")
]
