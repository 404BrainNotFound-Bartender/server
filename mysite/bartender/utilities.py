import queue
from threading import Thread
import time
import numpy as np
from .PID import PIDController


to_make_queue = queue.Queue()
making_drink = False
in_manual_mode = False
can_make_drink = False
manual_id = ""


def recipe_to_json(recipe):
    can_make = True
    ingre_count = 0
    parts = {}

    for com in recipe.ingredients.all():
        if not com.ingredient.attached_to_system and com.ingredient.est_remaining > 0:
            can_make = False
        parts[ingre_count] = {"amount": com.amount, "ingredient": com.ingredient.id}
        ingre_count += 1

    average = 0
    ratings = [r.rating for r in recipe.ratings.all()]
    if len(ratings) > 0:
        average = np.average(ratings)

    return {
        "name": recipe.name,
        "id": recipe.id,
        "parts": parts,
        "canMake": can_make,
        "serving": recipe.severing_size,
        "rating": average,
    }


def ingredient_to_dict(ingredient):
    alt_count = 0
    alts = {}
    for alt in ingredient.alternatives.all():
        alts[alt_count] = alt
        alt_count += 1
    return {
        "id": ingredient.id,
        "name": ingredient.name,
        "maker": ingredient.maker,
        "density": ingredient.density,
        "attached": ingredient.attached_to_system,
        "remaining": ingredient.est_remaining,
        "proof": ingredient.proof,
        "alternatives": alts,
        "pump": ingredient.assigned_pump.id
    }


def make_drink():
    global making_drink
    while True:
        if not in_manual_mode and not to_make_queue.empty():
            making_drink = True
            to_make = to_make_queue.get()
            to_make.making = True
            to_make.save()
            time.sleep(len(to_make.recipe.ingredients.all()) * 20)
            to_make.made = True
            to_make.save()
            print(to_make.amount)
            making_drink = False


def train_pid(ingredient, number_of_cycle, glass_size):
    train_pid_process = Thread(target=PIDController.train_pid, args=(
        number_of_cycle, ingredient.pid_p, ingredient.pid_i, ingredient.pid_d,
        glass_size))
    train_pid_process.start()
    train_pid_process.join()


to_make_process = Thread(target=make_drink)
to_make_process.start()
