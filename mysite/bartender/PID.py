from time import time
from math import pi


class PIDController:
    def __init__(self, p_const, i_const, d_const, target):
        self.k_p_const = p_const
        self.k_i_const = i_const
        self.k_d_const = d_const
        self.target = target
        self.total_error = 0
        self.last_error = 0
        self.last_run_time = 0

    @staticmethod
    def get_current_time():
        return int(round(time() * 1000))

    def compute(self, current_value):
        now = PIDController.get_current_time()
        time_change = now - self.last_run_time

        error = self.target - current_value
        self.total_error += (error * time_change)
        error_change_rate = (error - self.last_error) / time_change

        self.last_error = error
        self.last_run_time = now

        return int(self.k_p_const * error + self.k_i_const * self.total_error + self.k_d_const * error_change_rate)

    @staticmethod
    def train_pid(num_cycles, ingredient, use_old=False, size_of_glass=0, target_input=100,
                  max_output=65535, min_output=0, loop_interval=100):
        global can_make_drink
        can_make_drink = True
        output = True
        output_value = max_output
        t1 = t2 = PIDController.get_current_time()
        max = -1000000
        min = 1000000

        k_p, k_i, k_d = ingredient.pid_p, ingredient.pid_i, ingredient.pid_d
        num_samples = num_cycles
        p_average = i_average = d_average = 0

        if use_old:
            p_average, i_average, d_average = k_p, k_i, k_d
            num_samples += 1

        microseconds = t_high = t_low = 0

        for count in range(num_cycles):
            prev_microseconds = microseconds
            microseconds = PIDController.get_current_time()
            time_change = microseconds - prev_microseconds
            data_in = 0  # TODO Add class to read sensors

            max = max(max, data_in)
            min = min(min, data_in)

            if output and data_in > target_input:
                output = False
                output_value = min_output
                t1 = PIDController.get_current_time()
                t_high = t1 - t2
                max = target_input
            if not output and data_in < target_input:
                output = True
                output_value = max_output
                t2 = time()
                t_low = t2 - t1

                ku = (4.0 * ((max_output - min_output) / 2.0) / (pi * (max - min) / 2.0))
                tu = t_low + t_high

                k_p_const = 0.2
                t_i_const = 0.5
                t_d_const = 0.33

                k_p = k_p_const * ku
                k_i = (k_p / (t_i_const * tu)) * loop_interval
                k_d = (t_d_const * k_p * tu) / loop_interval

                if count > 1:
                    p_average += k_p
                    i_average += k_i
                    d_average += k_d

        ingredient.pid_p = p_average / num_samples
        ingredient.pid_i = i_average / num_samples
        ingredient.pid_d = d_average / num_samples
        can_make_drink = False
