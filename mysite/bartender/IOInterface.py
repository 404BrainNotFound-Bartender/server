import board
import busio
import digitalio
import adafruit_tlc5947
import adafruit_ads1x15.single_ended as ads1x15


class Interface:
    __spi__ = busio.SPI(clock=board.SCK, MOSI=board.MOSI)
    __i2c__ = busio.I2C(board.SCL, board.SDA)
    __latch__ = digitalio.DigitalInOut(board.D5)
    __tlc5947__ = adafruit_tlc5947.TLC5947(__spi__, __latch__)
    __motor_pins__ = [__tlc5947__.create_pwm_out(i) for i in range(24)]
    __ads__ = ads1x15.ADS1015(__i2c__)

    @staticmethod
    def set_pin(pin, cycle):
        global __motor_pins__
        __motor_pins__[pin].duty_cycle = cycle
