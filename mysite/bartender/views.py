from math import isclose

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import *
from .utilities import *

from uuid import uuid4


# Create your views here.

@csrf_exempt
def add_ingredient(request):
    if request.method == "POST":
        data = json.loads(request.body)
        ingredient = Ingredient()
        ingredient.name = data["name"]
        ingredient.density = data["density"]
        ingredient.est_remaining = data["remaining"]
        ingredient.attached_to_system = data["attached"]
        if ingredient.attached_to_system:
            ingredient.assigned_pump = Pump.objects.get(pk=data["pump"])
        ingredient.proof = data["proof"]
        ingredient.maker = data["maker"]
        # ingredient.image = data["image"]
        for ingre in Ingredient.objects.all():
            if isclose(ingredient.density, ingre.density):
                ingredient.pid_p = ingre.pid_p
                ingredient.pid_i = ingre.pid_i
                ingredient.pid_d = ingre.pid_d
                break
        ingredient.save()
        return JsonResponse(ingredient.to_dic())
    return JsonResponse({})


@csrf_exempt
def save_ingredient(request, id):
    if request.method == "POST":
        data = json.loads(request.body)
        ingredient = Ingredient.objects.get(pk=id)
        ingredient.name = data["name"]
        ingredient.est_remaining = data["remaining"]
        ingredient.assigned_motor = Pump.objects.get(pk=data["pump"])
        ingredient.attached_to_system = data["attached"]
        ingredient.proof = data["proof"]
        ingredient.maker = data["maker"]
        if ingredient.density != data["density"]:
            ingredient.density = data["density"]
            for ingre in Ingredient.objects.all():
                if isclose(ingredient.density, ingre.density):
                    ingredient.pid_p = ingre.pid_p
                    ingredient.pid_i = ingre.pid_i
                    ingredient.pid_d = ingre.pid_d
                    break
        for alt in data["alternatives"]:
            ingredient.alternatives.add(Ingredient.objects.get(pk=alt))
        ingredient.save()
    return JsonResponse({})


def get_ingredients(request):
    ingredients = Ingredient.objects.all()
    data = {}
    count = 0
    for ingredient in ingredients:
        data[count] = ingredient.to_dic()
        try:
            data[count]["pump"] = ingredient.assigned_pump.id
        except AttributeError:
            pass
        count += 1
    return JsonResponse(data)


def get_ingredient(request, ingredient_id):
    ingredient = Ingredient.objects.get(pk=ingredient_id)
    return JsonResponse(ingredient.to_json())


def assign_ingredient_to_motor(request, ingredient_id, motor_id):
    for ingre in Ingredient.objects.get(assigned_motor=motor_id):
        ingre.attached_to_system = False
        ingre.save()
    ingredient = Ingredient.objects.get(pk=ingredient_id)
    ingredient.attached_to_system = True
    ingredient.assigned_motor = motor_id
    ingredient.save()


def get_all_recipes(request):
    recipes = Recipe.objects.all()
    recipes_dict = {}
    count = 0
    for recipe in recipes:
        recipes_dict[count] = recipe_to_json(recipe)
        count += 1
    return JsonResponse(recipes_dict)


@csrf_exempt
def update_recipe(request):
    if request.method == "POST":
        data = json.loads(request.body)
        recipe = Recipe.objects.get(pk=data["id"])
        recipe.name = data["name"]
        recipe.severing_size = data["serving"]
        for comp in recipe.ingredients.add():
            comp.delete()
        recipe.save()
        for component in data["components"]:
            comp = Component()
            comp.ingredient = Ingredient.objects.get(pk=component["id"])
            comp.amount = component["amount"]
            comp.save()
            recipe.ingredients.add(comp)
        recipe.save()

    return JsonResponse({})


@csrf_exempt
def add_recipe(request):
    if request.method == "POST":
        data = json.loads(request.body)
        recipe = Recipe()
        recipe.name = data["name"]
        recipe.save()
        for component in data["components"]:
            comp = Component()
            comp.ingredient = Ingredient.objects.get(pk=component["id"])
            comp.amount = component["amount"]
            comp.save()
            recipe.ingredients.add(comp)
        recipe.save()
        return JsonResponse(recipe_to_json(recipe))
    return JsonResponse({})


def make_recipe(request, recipe_id, amount):
    to_make = MakeRecipeRequest()
    to_make.recipe = Recipe.objects.get(pk=recipe_id)
    to_make.amount = amount / 100
    to_make_queue.put(to_make)
    to_make.positions_in_queue = to_make_queue.qsize()
    to_make.save()
    return JsonResponse({
        "position": to_make.positions_in_queue,
        "id": to_make.id,
        "recipe": to_make.recipe.id,
        "amount": to_make.amount,
    })


def get_image(request, type, id):
    if type == 0:
        ingredient = Ingredient.objects.get(pk=id)
        image = ingredient.image
        image = image.replace("[", "").replace("]", "")
        print(len(image))
        data = image.split(",")
        return JsonResponse({"image": data})
    else:
        recipe = Recipe.objects.get(pk=id)
        return JsonResponse({"image": recipe.image})


def get_pumps(request):
    count = 0
    payload = {}
    for pump in Pump.objects.all():
        payload[count] = pump.to_dict()
        count += 1
    return JsonResponse(payload)


@csrf_exempt
def add_pump(request):
    if request.method == "POST":
        data = json.loads(request.body)
        pump = Pump()
        pump.name = data["name"]
        pump.pin = data["pin"]
        pump.save()
        return JsonResponse(pump.to_dict())


@csrf_exempt
def save_pump(request, id):
    if request.method == "POST":
        data = json.loads(request.body)
        pump = Pump.objects.get(pk=id)
        pump.name = data["name"]
        pump.pin = data["pin"]
        pump.save()
        return JsonResponse(pump.to_dict())


def get_drink_status(request, id):
    drink = MakeRecipeRequest.objects.get(pk=id)
    return JsonResponse({
        "made": drink.made,
        "status": drink.making,
    })


def get_bartender_status(request):
    return JsonResponse({
        "manualMode": in_manual_mode,
        "makingDrink": making_drink,
        "numberDrinksToMake": to_make_queue.qsize()
    })


def enter_manual_mode(request):
    payload = {}
    global in_manual_mode
    global manual_id

    if to_make_queue.empty() and manual_id == "":
        payload["entered"] = True
        in_manual_mode = True
        manual_id = str(uuid4())
        payload["manualId"] = manual_id
    else:
        payload["entered"] = False
        payload["queue"] = to_make_queue.qsize()
    return JsonResponse(payload)


def dispense_ingredient(request, manual_code, ingredient, amount):
    global manual_id
    if manual_id == manual_code:
        print(amount)
    return JsonResponse({})


def leave_manual_mode(request, manual_code):
    global in_manual_mode
    global manual_id
    if manual_id == manual_code:
        in_manual_mode = False
        manual_id  = ""
    return JsonResponse({})


def train_ingredient_pid(request, ingredient_id, number_of_cycle, size_of_glass):
    pass


def delete_recipe(request, recipe_id):
    recipe = Recipe.objects.get(pk=recipe_id)
    for comp in recipe.ingredients.all():
        comp.delete()
    recipe.delete()
    return JsonResponse({})


def rate_recipe(request, recipe_id, rating):
    recipe = Recipe.objects.get(pk=recipe_id)
    new_rating = RecipeRating()
    new_rating.rating = rating / 100
    new_rating.save()
    recipe.ratings.add(new_rating)
    recipe.save()
    return JsonResponse({})


def connection_test(request):
    return JsonResponse({"status": True})